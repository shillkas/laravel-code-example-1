<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController')->name('index');

Route::middleware(['auth'])->group(function () {
    // Тут были другие routes требующие авторизации
});

// страница поиска - SearchController
Route::get('/search', 'SearchController@SearchPage')->name('search.page');

<?php

namespace App;

use EloquentFilter\Filterable;
use Hamcrest\Core\Set;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Settings;
use App\Order;
use App\User;

class Product extends Model
{
    use Filterable;

    const SALE_STATUS = 'sale';
    const SOLD_STATUS = 'sold';
    const MODERATION_STATUS = 'moderation';
    const REMOVED_STATUS = 'removed';

    public function owner()
    {
        return $this->belongsTo('App\User');
    }

    public function reviews()
    {
        return $this->morphMany('App\Review', 'item');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'products_categories');
    }

    public function size()
    {
        return $this->belongsTo('App\Size');
    }

    public function condition()
    {
        return $this->belongsTo('App\Condition');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function photos()
    {
        return $this->hasMany('App\ProductPhotos')
            ->orderBy('main_photo', 'desc');
    }

    public function usersFavorite()
    {
        return $this->morphToMany('App\User', 'item', 'user_saves');
    }

    public function usersFavoriteCount()
    {
        return $this->usersFavorite()->count();
    }

    public function getFullTitleAttribute()
    {
        return $this->brand . ' ' . $this->model;
    }

    public function getFormattedCreatedAtAttribute()
    {
        return $this->created_at->format('d.m.Y H:i');
    }

    public static function getNewestProductsByCategories($categoriesIds)
    {
        return self::join('products_categories', 'products.id', '=', 'products_categories.product_id')
            ->select('products.*')
            ->whereIn('products_categories.category_id', $categoriesIds)
            ->where('status', self::SALE_STATUS)
            ->orderBy('created_at', 'desc');
    }

    public function getCreatedAtAgoAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
    }

    public function getAllTypeCosts($mailDelivery = true)
    {
        if ($mailDelivery && $this->mail_delivery) {
            $deliveryCost = $this->cost_delivery;
        } else {
            $deliveryCost = 0;
        }

        $comissionValue = (float) Settings::firstWhere('title', Settings::COMISSION_TITLE)->value;

        $comissionCost = round($this->cost * $comissionValue);

        return [
            'product_cost' => $this->cost,
            'delivery_cost' => $deliveryCost,
            'comission_value' => $comissionValue,
            'comission_cost' => $comissionCost,
            'all_cost' => $this->cost + $deliveryCost + $comissionCost
        ];
    }

    public function getActualQuantityCount()
    {
        $quantityStarted = $this->quantity;

        $activeOrdersCount = Order::getActiveOrdersByProduct($this)->count();

        return $quantityStarted - $activeOrdersCount;
    }

    public function isActive()
    {
        return $this->isSale() || $this->isModeration();
    }

    public function isPublic()
    {
        return $this->isSale() || $this->isSold();
    }

    public function isSale()
    {
        return $this->status == self::SALE_STATUS ? true : false;
    }

    public function isModeration()
    {
        return $this->status == self::MODERATION_STATUS ? true : false;
    }

    public function isSold()
    {
        return $this->status == self::SOLD_STATUS ? true : false;
    }

    public function isRemoved()
    {
        return $this->status == self::REMOVED_STATUS ? true : false;
    }

    static public function getAllStatus(){
        return [self::SALE_STATUS, self::SOLD_STATUS, self::MODERATION_STATUS, self::REMOVED_STATUS];
    }
}

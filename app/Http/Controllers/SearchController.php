<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Country;
use App\Product;
use App\City;

class SearchController extends Controller
{
    const LIMIT_SEARCH_PRODUCTS = 15;

    public function SearchPage(Request $request)
    {
        // Для фильтрации используется tucker-eric/eloquentfilter
        // Смотреть в /app/ModelFilters/ProductFilter.php
        $products = Product::filter($request->all())->simplePaginateFilter(self::LIMIT_SEARCH_PRODUCTS);

        // На странице есть ajax загрузка товаров при изменении фильтра
        // А также ajax подгузка товаров при скролле
        if ($request->ajax()) {
            return response()->json([
                'html' => view('product/products_layout', ['products' => $products])->render(),
                'count' => $products->count(),
                'page' => $products->currentPage(),
                'hasMore' => $products->hasMorePages()
            ]);
        } else {
            // Определение активной главной категории по выбранным дочерним
            // Или по указанному алиасу главной категории
                $type = $request->input('type');

                $categoryActiveAlias = Category::DEFAULT_SEARCH_CATEGORY_ALIAS;

                if ($type !== null && ($category = Category::firstWhere('alias', $type))) {
                    $categoryActiveAlias = $type;
                } else {
                    if ($request->categories && !empty($request->categories)) {
                        $firstCategory = $request->categories[0];

                        if ($firstCategory = Category::find($firstCategory)) {
                            $categoryActiveAlias = $firstCategory->mainParent()->alias;
                        }
                    }
                }

            return view('search/search', [
                'countries' => Country::orderBy('title', 'asc')->get(),
                'products' => $products,
                'choosed_city' => City::find($request->input('city')),
                'categoryActiveAlias' => $categoryActiveAlias
            ]);
        }
    }
}

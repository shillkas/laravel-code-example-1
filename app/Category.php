<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const DEFAULT_SEARCH_CATEGORY_ALIAS = 'men';

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id')
            ->where('active', 1);
    }

    public function mainParent()
    {
        $category = $this;

        while (true) {
            if ($category->parent === null) {
                return $category;
            }

            $category = $category->parent;
        }
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id')
            ->where('active', 1);
    }

    public function childrenRecursive()
    {
        return $this->children()
            ->with('childrenRecursive');
    }

    public function childrenRecursiveWithData()
    {
        return $this->children()
            ->with('conditions')
            ->with('sizes')
            ->with('childrenRecursiveWithData');
    }

    public function getChildrenIdsRecursive($withParent = false)
    {
        $categoriesIds = $this->getChildrenIdsRecursiveHelper($this->childrenRecursive()->get()->toArray());

        if ($withParent) {
            $categoriesIds[] = $this->id;
        }

        return $categoriesIds;
    }

    protected function getChildrenIdsRecursiveHelper($categories)
    {
        $res = [];
        foreach ($categories as $category) {
            $res[] = $category['id'];
            $res = array_merge($res, $this->getChildrenIdsRecursiveHelper($category['children_recursive']));
        }

        return $res;
    }

    public function sizes()
    {
        return $this->belongsToMany('App\Size', 'categories_sizes');
    }

    public function conditions()
    {
        return $this->belongsToMany('App\Condition', 'categories_conditions');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'products_categories');
    }

    public function getCreatedAtAgoAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
    }
}

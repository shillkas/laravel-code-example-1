<?php

namespace App\ModelFilters;

use App\Product;
use App\Category;
use EloquentFilter\ModelFilter;

class ProductFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    protected $drop_id = false;
    protected $camel_cased_methods = false;
    protected $blacklist = ['onSale'];

    const BOOLEAN_WHITE_LIST_TRUE = [1, 'true', 'on'];
    const SORT_AVAILABLE_COLUMNS = ['created_at', 'cost'];
    const SORT_DIRS = ['desc', 'DESC', 'asc', 'ASC'];

    public function setup()
    {
        $this->onSale();

        // Handling of type input
        if ($this->input('categories') === null) {
            $type = ($this->input('type') !== null) ?
                $this->input('type') : Category::DEFAULT_SEARCH_CATEGORY_ALIAS;

            if (($category = Category::firstWhere('alias', $type)) &&
                !empty($categoryChildren = $category->getChildrenIdsRecursive())) {
                $this->categories($categoryChildren);
            }
        }
    }

    public function query($search)
    {
        $this->where(function($query) use ($search) {
                    return $query->where('model', 'LIKE', '%'.$search.'%')
                        ->orWhere('brand', 'LIKE', '%'.$search.'%');
                });
    }

    public function safe_buy($val)
    {
        if (in_array($val, self::BOOLEAN_WHITE_LIST_TRUE)) {
            return $this->where('safe_buy', true);
        }
    }

    public function categories($categories)
    {
        $this->related('categories', function($query) use ($categories) {
            return $query->whereIn('category_id', $categories);
        });
    }

    public function min_price($minPrice)
    {
        return $this->where('cost', '>=', $minPrice);
    }

    public function max_price($maxPrice)
    {
        return $this->where('cost', '<=', $maxPrice);
    }

    public function sizes($sizes)
    {
        return $this->whereIn('size_id', $sizes);
    }

    public function conditions($conditions)
    {
        return $this->whereIn('condition_id', $conditions);
    }

    public function country($countryId)
    {
        return $this->where('country_id', $countryId);
    }

    public function city($cityId)
    {
        return $this->where('city_id', $cityId);
    }

    public function onSale()
    {
        return $this->where('status', Product::SALE_STATUS);
    }

    public function sort($val)
    {
        if (in_array($val, self::SORT_AVAILABLE_COLUMNS)) {
            if (in_array($this->input('sort_dir'), self::SORT_DIRS)) {
                $sortDir = $this->input('sort_dir');
            } else {
                $sortDir = 'asc';
            }

            return $this->orderBy($val, $sortDir);
        }
    }
}
